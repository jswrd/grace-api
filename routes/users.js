const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


// user Signup
router.post('/signup', (req, res, next) => {
    try {
        if(req.body.first_name === '' || req.body.last_name === '' || req.body._password === '' || req.body.phone_number === null || req.body.street_address === '' || req.body.city === '' || req.body.pincode === null || req.body.state === '' || req.body.country === '' || req.body.email === '' ){
            res.status(500).json({
                message: 'Try Again'
            })
        }else{
            bcrypt.hash(req.body._password, 10, (err, hash) => {
                if(err){
                    return res.status(500).json({
                        error: err,
                    });
                }else{
                    let user = {
                        first_name: req.body.first_name,
                        last_name: req.body.last_name,
                        _password: hash,
                        phone_number: req.body.phone_number,
                        street_address: req.body.street_address,
                        city: req.body.city,
                        pincode: req.body.pincode ,
                        state: req.body.state,
                        country: req.body.country,
                        email: req.body.email, 
                    }
                    let sql = "INSERT INTO users SET ?"
                    state.connection.query(sql, user, function (err, res) {             
                            if(err) {
                                console.log(err);
                            }
                            else {
                                console.log('user added');
                                
                            }
                    }); 
                }
            })
        }
        
        
    }
    catch(err) {
        console.log(err);
    }

    
       res.status(201).json({
        message: 'user added',
    });
});



// User Login
router.post('/login', (req, res, next) => {
    const _email = req.body._email;
        state.connection.query('SELECT * FROM users WHERE email = ?', [_email], (error, result, field) => {
        if( result.length > 0){
                bcrypt.compare( req.body._password, result[0]._password, (err, results) => {
                if(err){
                    return res.status(401).json({
                        message: 'Login Failed'
                    })
                }
                if(results){
                    const token = jwt.sign({
                        userid: result[0]._id,
                        userfirstname: result[0].first_name,
                        userlastname: result[0].last_name,
                        
                    }, 
                    'secret',
                    {
                        expiresIn: '1h',
                    }
                    );
                    return res.status(200).json({
                        message: 'Login Successful',
                        token: token,
                    })
                }
                res.status(401).json({
                    message: 'Incorrect Password',
                })
            })



        }else{
            res.status(401).json({
                message: 'Incorrect Email.'
            })
        }
        
    })
})


// Get Delivery address
// NOTE: Need to add billing address
router.post('/getAddress', (req, res, next) => {
    const user_id = req.body.user_id
    
        state.connection.query(`SELECT * FROM users WHERE _id = ?`,[user_id], function(error, users, fields) {
        
        
        res.status(200).send(users);
        
    });
});


// Change address
router.post('/changeAddress', (req, res, next) => {
    const user_id = req.body.user_id;

    let user = {
        street_address: req.body.street_address,
        city: req.body.city,
        pincode: req.body.pincode ,
        state: req.body.state,
        country: req.body.country, 
    }
    let sql = `UPDATE users SET ? WHERE _id = ${user_id}` 
    state.connection.query(sql, user, function (err, res) {             
            if(err) {
                console.log(err);
            }
            else {
                console.log('Delivery address updated.');
                
                
            }
    }); 

    res.status(201).json({
        message: 'Delivery address updated.'
    })
});



// post enquiry From user UI about refund status
router.post('/enquiry', (req, res, next) => {
    try {
        let enquiry = {
            full_name: req.body.full_name, 
            email: req.body.email, 
            phone: req.body.phone, 
            enquiry: req.body.enquiry, 
            
        }
    
        let sql = "INSERT INTO enquiry SET ?"
        state.connection.query(sql, enquiry, function (err, res) {             
                if(err) {
                    console.log(err);
                }
                else {
                    console.log('enquiry added');
                    
                    
                }
        });

    }
    catch(err){
        console.log(err);
        

    }
     

    res.status(201).json({
        message: 'enquiry added',
    });
});



// change user details

router.post('/editDetails', (req, res, next) => {
    

    const _id = req.body.user_id
    const first_name = req.body.first_name;
    const last_name = req.body.last_name;
    const phone_number = req.body.phone_number;
    const email = req.body.email;

    state.connection.query(`UPDATE users SET first_name = ?,last_name = ?,phone_number = ?,email = ? WHERE _id = ?`,[first_name, last_name, phone_number, email, _id], function(error, users, fields) {
        
        console.log('user details updated');
        
        res.status(200).json({
            "message": "Done"
        });
        
    });
    
})



// Reset password

router.post('/resetPassword', (req, res, next) => {
    const user_id = req.body.user_id;
    const oldPassword = req.body.password;
    const newPassword = req.body.new_pass;

    state.connection.query('SELECT * FROM users WHERE _id = ?', [user_id], (error, result, field) => {
        
        
            bcrypt.compare( oldPassword, result[0]._password, (err, results) => {
            if(err){
                
                res.status(401).send(err)
            }
            if(results){

                bcrypt.hash(newPassword, 10, (err, hash) => {

                    state.connection.query(`UPDATE users SET _password = ? WHERE _id = ?`,[hash, user_id], function(error, users, fields) {
        
                        console.log('password changed');
                        
                        
                    });
                })

                


               
                return res.status(200).json({
                    "message": 'Password successfully changed',
                })
            }else{
                res.status(401).json({
                    message: 'Wrong Password'
                })
            }
        })



    
    })


})








module.exports = router;