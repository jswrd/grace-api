const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;
const multer = require('multer');
const checkAuth = require('../middleware/check-auth')

// init the multer storage for image storing
const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './itemImages/')
    },
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
});

//filter for uploaded image files
const fileFilter = (req, file, cb) =>{
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true);
    }else{
        cb(new Error('File format not supported!'), false);
    }
}

const upload = multer({storage: storage, limits: {fileSize: 1024 * 1024 * 5}, fileFilter: fileFilter});



router.get('/', (req, res, next) => {
    
        res.status(200).json({
        message: 'handling GET request to /products',
    });
});



router.post('/', (req, res, next) => {
    
    res.status(200).json({
    message: 'handling POST request to /products',
});
});





// router.patch('/:productId', (req, res, next) => {
//     res.status(200).json({
//         message: 'product updated',
//     });
// });





// add item to the database

router.post('/addItem', upload.single('file'), (req, res, next) => {
    try {
        // console.log(req.file);
        
        let item = {
            main_product_id: req.body.mainProduct, 
            sub_product_id: req.body.subProduct, 
            item_type: req.body.item_type, 
            item_name: req.body.item_name, 
            item_quantity: req.body.item_quantity, 
            item_description: req.body.item_description, 
            item_price: req.body.item_price,
            item_image: req.file.path,
            item_rating: req.body.item_rating,
            availability: 1,
        }

        let sql = "INSERT INTO all_items SET ?"
        state.connection.query(sql, item, function (err, res) {             
                if(err) {
                    console.log(err);
                }
                else {
                    console.log('item added');
                    
                    
                }
        }); 
        
           

    }
    catch(err) {
        console.log(err);
    }

    res.status(201).json({
        message: 'item added',
    });
    
});




// router.get('/getProducts', (req, res, next) => {
    
//     state.connection.query('SELECT * FROM main_products LEFT OUTER JOIN sub_products ON main_products.serial_no=sub_products.serial_no', (error, results, fields) => {
//         res.status(200).send(results);
        
//     })

// });






// delete an item from the database
router.delete('/items/:itemId', (req, res, next) => {

    const item_id = req.params.itemId;
    const availability = 0;
    

    state.connection.query(`UPDATE all_items SET availability = ? WHERE item_id = ?`, [availability, item_id], function(error, results, fields) {
        res.status(200).json({
            message: 'item deleted',
        });
    });
    
});



// Get specific Item details
router.get('/items/:itemId', (req, res, next) => {
    const item_id = req.params.itemId;
    state.connection.query('SELECT * FROM all_items WHERE item_id = ?', [item_id], function(error, items, fields) {
        res.status(200).send(items[0])
        
    });

});




// Get priority items
router.get('/priorityItems', (req, res, next) => {

    const availability = 1;

    state.connection.query('SELECT * FROM all_items WHERE availability = ? ORDER BY item_rating DESC LIMIT 3', [availability], function(error, items, fields) {
        
        
        res.status(200).send(items);
    });

});


// Get items from sub product
router.post('/subProductItems', (req, res, next) => {
    
    const subProduct = req.body.subProduct
    const availability = 1;

    state.connection.query('SELECT * FROM sub_products WHERE sub_product_name = ? ', [subProduct], function(error, item, fields) {
        
        const subProductId = item[0].sub_product_id;
        

        state.connection.query('SELECT * FROM all_items WHERE sub_product_id = ? AND availability = ? ', [subProductId, availability], function(error, items, fields) {

            res.status(200).send(items);
        })
        
        
        
        
    });

});


// Get items from main product
router.post('/mainProductItems', (req, res, next) => {
    
    const mainProduct = req.body.mainProduct
    const availability = 1;

    state.connection.query('SELECT * FROM main_products WHERE main_product_name = ? ', [mainProduct], function(error, item, fields) {
        
        const mainProductId = item[0].main_product_id;
        

        state.connection.query('SELECT * FROM all_items WHERE main_product_id = ? AND availability = ? ', [mainProductId, availability], function(error, items, fields) {

            res.status(200).send(items);
        })
        
        
        
        
    });

});


//Search products
router.post('/search', (req, res, next) => {

    const query = req.body.query;
    const availability =1;
    

    state.connection.query(`SELECT * FROM all_items WHERE INSTR(item_name, '${query}') > 0 AND availability = ?`,[availability], function(error, items, fields) {
        res.status(200).send(items);
    });

});


//update item qunatity after order confirmation
router.post('/updateQuantity', (req, res, next) => {

    const id = req.body.transId;
    
    
        state.connection.query(`SELECT * FROM orders WHERE transaction_id = ?`,[id], function(error, items, fields) {
        
            items.forEach(item => {
                console.log(item.item_id, item._quantity);
                const item_id = item.item_id;
                const ordered_quantity = item._quantity

                state.connection.query(`SELECT * FROM all_items WHERE item_id = ?`,[item_id], function(error, item, fields) {
        
                    const item_quantity = item[0].item_quantity;
        
                    const rem_quantity = item_quantity - ordered_quantity;    
                   
                    state.connection.query(`UPDATE all_items SET item_quantity = ? WHERE item_id = ?`,[rem_quantity, item_id], function(error, item, fields) {
        
                        console.log('item quantity updated');
                        
                          
                            
                        
                    
                });
                    
                
            });
                
            });

            res.status(201).json({
                message: 'item quantity updated'
            })
        
    });

});























module.exports = router;