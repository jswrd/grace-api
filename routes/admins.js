const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')




router.post('/', (req, res, next) => {
   console.log(req.body);
   res.status(201).json({
       message: "hello world."
   })
   
});


// Admin Signup
router.post('/signup', (req, res, next) => {
    try {
        if(req.body.full_name === '' || req.body.user_name === '' || req.body._password === ''){
            res.status(500).json({
                message: 'Try Again'
            })
        }else{
            bcrypt.hash(req.body._password, 10, (err, hash) => {
                if(err){
                    return res.status(500).json({
                        error: err,
                    });
                }else{
                    let admin = {
                        full_name: req.body.full_name,
                        user_name: req.body.user_name,
                        _password: hash 
                    }
                    let sql = "INSERT INTO admin_1 SET ?"
                    state.connection.query(sql, admin, function (err, res) {             
                            if(err) {
                                console.log(err);
                            }
                            else {
                                console.log('admin added');
                                
                            }
                    }); 
                }
            })
        }
        
        
    }
    catch(err) {
        console.log(err);
    }

    
       res.status(201).json({
        message: 'admin added',
    });
});





// Admin Login
router.post('/login', (req, res, next) => {
    const user_name = req.body.user_name;
    state.connection.query('SELECT * FROM admin_1 WHERE user_name = ?', [user_name], (error, result, field) => {
        if( result.length > 0){
                bcrypt.compare( req.body._password, result[0]._password, (err, results) => {
                if(err){
                    return res.status(401).json({
                        message: 'Login Failed'
                    })
                }
                if(results){
                    const token = jwt.sign({
                        userid: result[0]._id,
                        userfullname: result[0].full_name,
                        
                    }, 
                    'secret',
                    {
                        expiresIn: '1h',
                    }
                    );
                    return res.status(200).json({
                        message: 'Login Successful',
                        token: token,
                    })
                }
                res.status(401).json({
                    message: 'Incorrect Password',
                })
            })



        }else{
            res.status(401).json({
                message: 'Incorrect Username.'
            })
        }
        
    })
})













// for admin ui new



//get all products

router.get('/allItems', (req, res, next) => {

    const availability = 1;
    
    state.connection.query('SELECT * FROM all_items WHERE availability = ?',[availability], (error, results, fields) => {
        res.status(200).send(results);
        
    })

});


//add terms, privacy, shipping
router.post('/editUserUi', (req, res, next) => {
    

    
    let details = {
        name: req.body.name,
        header: req.body.header,
        para: req.body.para,
         
    }
    let sql = "INSERT INTO details SET ?"
    state.connection.query(sql, details, function (err, res) {             
            if(err) {
                console.log(err);
            }
            else {
                console.log('details added');
               
                
            }
        })


        res.status(201).json({
            message: 'details added'
        })

        

            

           
    }); 
    
   
    
    
    
   //get terms and conditions
    router.get('/getTerms', (req, res, next) => {

        const name = 'terms'
    
        state.connection.query('SELECT * FROM details WHERE name = ?', [name], function(error, items, fields) {
                    res.status(200).send(items)
                    
                });
    
    });



    //get privacy
    router.get('/getPrivacy', (req, res, next) => {

        const name = 'privacy'
    
        state.connection.query('SELECT * FROM details WHERE name = ?', [name], function(error, items, fields) {
                    res.status(200).send(items)
                    
                });
    
    });


    //get shipping
    router.get('/getShipping', (req, res, next) => {

        const name = 'shipping'
    
        state.connection.query('SELECT * FROM details WHERE name = ?', [name], function(error, items, fields) {
                    res.status(200).send(items)
                    
                });
    
    });



// delete a header-paragraph pair
router.post('/deletePair', (req, res, next) => {

    const id = req.body.id;

    console.log(id);
    

    state.connection.query('DELETE FROM details WHERE id = ?', [id], function(error, results, fields) {
        res.status(200).json({
            message: 'pair deleted',
        });
    });
    
});




//get customers
router.get('/getCustomers', (req, res, next) => {


    state.connection.query('SELECT * FROM users', function(error, users, fields) {
                res.status(200).send(users)
                
            });

});



// update delivery status
//NOTE: API of the third party delivery partner
router.post('/deliveryStatus', (req, res, next) => {

    const order_id = req.body._id;
    const status = req.body.status;
    
    state.connection.query(`UPDATE orders SET delivery_status = ? WHERE _id = ?`,[status, order_id], function(error, items, fields) {
        
        console.log('delivery status updated');
        
        res.status(200).json({
            "message": "delivery status updated"
        });
        
    });
    


});




// update orders after order delivered
router.post('/orderDelivered', (req, res, next) => {

    const order_id = req.body._id;
    const status = 'Order Delivered';
    const date = new Date();
    
    state.connection.query(`UPDATE orders SET _status = ?, delivery_status = ?, delivery_date = ? WHERE _id = ?`,[status, status, date, order_id], function(error, items, fields) {
        
        console.log('order status updated');
        
        res.status(200).json({
            "message": "order status updated"
        });
        
    });
    
});



// get details for delivered orders
router.post('/deliveredOrderDetails', (req, res, next) => {
    const user_id = req.body.user_id
    // console.log(user_id);
    
    state.connection.query("SELECT * FROM orders JOIN all_items ON orders.item_id = all_items.item_id WHERE user_id = ? && _status = 'Order Delivered' ORDER BY _id DESC",[user_id], function(error, orders, fields) {


        res.status(200).send(orders);

        // res.status(200).send(users);
        // console.log(users);
        
    });
});


//show refund requests
router.post('/getRefund', (req, res, next) => {
    const user_id = req.body.user_id
    state.connection.query("SELECT * FROM orders JOIN refund_request ON orders._id = refund_request.order_id WHERE user_id = ?",[user_id], function(error, orders, fields) {


        res.status(200).send(orders);

        // res.status(200).send(users);
        // console.log(users);
        
    });
});


// update refund status
router.post('/refundStatus', (req, res, next) => {

    const order_id = req.body.order_id;
    const status = 'Payment Refunded';
    
    state.connection.query(`UPDATE refund_request SET status = ? WHERE order_id = ?`,[status, order_id], function(error, items, fields) {
        
        console.log('Refund successful');
        
        res.status(200).json({
            "message": "Refund Successful"
        });
        
    });
    


});












module.exports = router;