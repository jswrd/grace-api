const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;
const checkAuth = require('../middleware/check-auth');
const keys = require('../config/keys')
const checksum_lib = require('../paytm/checksum/checksum')





router.get('/', (req, res, next) => {
    res.send('Hello World')
});

// Add an item to cart
router.post('/addToCart', (req, res, next) => {
    const item_id = req.body.item_id
    const user_id = req.body.user_id
    

    // console.log(item_id, user_id);
    

    state.connection.query(`SELECT * FROM orders WHERE item_id = ? && user_id = ? && _status = 'Added to cart' `,[item_id, user_id], function(error, items, fields) {
        // console.log(items.length);
        if(items.length === 0){
            state.connection.query("SELECT * FROM all_items WHERE item_id = ?",[item_id], function(error, item, fields) {

                const item_price = item[0].item_price
                const _quantity = 1
                let order = {
                    user_id: req.body.user_id,
                    item_id: req.body.item_id,
                    _status: req.body._status,
                    _quantity: 1,
                    total_price: _quantity * item_price
                }
            
                
                
                let sql = "INSERT INTO orders SET ?"
                state.connection.query(sql, order, function (err, res) {             
                        if(err) {
                            console.log(err);
                        }
                        else {
                            console.log('item added to the cart');
                            
                        }
                });
            
                res.status(200).json({
                    message: 'item added to the cart', 
                });
                
                
            });

        }else{
            res.status(200).json({
                message: 'item has been already added', 
            });
        }

        
    })




   

    
});


// Show items in cart
router.post('/showCart', (req, res, next) => {
    const user_id = req.body.user_id
    state.connection.query("SELECT * FROM orders JOIN all_items ON orders.item_id = all_items.item_id WHERE user_id = ? && _status = 'Added to cart'",[user_id], function(error, items, fields) {

        res.status(200).send(items);
        
    });
});

// get details for payment
router.post('/paymentDetails', (req, res, next) => {
    const user_id = req.body.user_id
    state.connection.query("SELECT * FROM orders JOIN users ON orders.user_id = users._id WHERE user_id = ? && _status = 'Added to cart'",[user_id], function(error, users, fields) {
        
        state.connection.query("SELECT SUM(total_price) as cost FROM orders WHERE user_id = ? && _status = 'Added to cart'",[user_id], function(error, sum, fields) {

            // res.status(200).send(sum);
            // console.log(sum[0]["cost"]);
            users.push({"cost":sum[0]["cost"]});
            
            // console.log(users);
            res.status(200).json({"users":users});
            
            
            
        });

        // res.status(200).send(users);
        // console.log(users);
        
    });
});


// get details for placed orders
router.post('/placedOrderDetails', (req, res, next) => {
    const user_id = req.body.user_id
    // console.log(user_id);
    
    state.connection.query("SELECT * FROM orders JOIN all_items ON orders.item_id = all_items.item_id WHERE user_id = ? && _status = 'Order Placed' ORDER BY _id DESC",[user_id], function(error, orders, fields) {


        res.status(200).send(orders);

        // res.status(200).send(users);
        // console.log(users);
        
    });
});


// get details for cancelled orders
router.post('/cancelledOrderDetails', (req, res, next) => {
    const user_id = req.body.user_id
    state.connection.query("SELECT * FROM orders JOIN all_items ON orders.item_id = all_items.item_id WHERE user_id = ? && _status = 'Order Cancelled' ORDER BY _id DESC",[user_id], function(error, orders, fields) {


        res.status(200).send(orders);

        // res.status(200).send(users);
        // console.log(users);
        
    });
});


// send refund request
router.post('/refundRequest', (req, res, next) => {
    
    
    let request = {
        order_id: req.body._id,
        cancellation_date: new Date(),
    }
    state.connection.query("SELECT * FROM refund_request WHERE order_id = ?",[req.body._id], function(error, requests, fields) {
        if(requests.length > 0){
            res.status(200).json({
                "message" : "refund request has been already sent"
            })
        }else{
            let sql = "INSERT INTO refund_request SET ?"
            state.connection.query(sql, request, function (err, res) {             
                    if(err) {
                        console.log(err);
                    }
                    else {
                        console.log('refund request added');
                        
                    }
            }); 
        
            res.status(200).json({
                "message" : "refund request sent"
            })

        }

    })
    




   
});



// get total price in cart
router.post('/getTotalPrice', (req, res, next) => {
    const user_id = req.body.user_id
    state.connection.query("SELECT SUM(total_price) as cost FROM orders JOIN all_items ON orders.item_id = all_items.item_id WHERE user_id = ? && _status = 'Added to cart'",[user_id], function(error, sum, fields) {

        res.status(200).send(sum);
        
    });
});


// Delete items in cart
router.delete('/deleteFromCart/:orderId', (req, res, next) => {
   
    const _id = req.params.orderId
       
     state.connection.query("DELETE FROM orders WHERE _id = ?;",[_id], function(error, items, fields) {
        if(!error){
            console.log('Item removed from cart');
            
            res.status(200).send("Removed from cart");
        }
        
        
        
    });
});


// Update items in cart
router.post('/updateCartItem', (req, res, next) => {
    const _id = req.body._id
    const _quantity = req.body.quantity

    state.connection.query("SELECT * FROM orders JOIN all_items ON orders.item_id = all_items.item_id WHERE _id = ?",[_id], function(error, item, fields) {
        
        // console.log(item[0]);

        const item_price = item[0].item_price;
        const total_price = _quantity * item_price;
        
        // console.log(total_price, _quantity, item_price);
        


        state.connection.query(`UPDATE orders SET _quantity = ${_quantity},total_price = ${total_price} WHERE _id = ?`,[_id], function(error, items, fields) {
        
            console.log('order updated');
            
            res.status(200).send('order updated');
            
        });
        
        
    });



  
});







// Payment Gateway

router.post('/payment', (req, res, next) => {
    // console.log(req.body);

    const params = req.body
    

    checksum_lib.genchecksum(params,keys.paytmKey, function(err,checksum){

        res.status(200).json({
            "checksum" : checksum
        })

      
    })


});



// Payment confirmation

router.post('/confirmation', (req, res, next) => {
    
    res.redirect('http://localhost:4200/#/payment-confirm');


});


// update order table after payment
// NOTE: Need to send an email on order confirmation

router.post('/confirmOrder', (req, res, next) => {

    const user_id = req.body.user_id;
    const str = 'Order Placed';
    const str1 = 'Added to cart'
    const date = new Date();
    
    const transaction_id = req.body.transId
  
    
    
    state.connection.query(`UPDATE orders SET _status = ?, transaction_id = ?, order_date = ? WHERE user_id = ? && _status = ?`,[str, transaction_id, date, user_id, str1], function(error, items, fields) {
        
        console.log('order placed');
        
        res.status(200).json({
            "message": "order placed"
        });
        
    });
    


});



// cancel an order

router.post('/cancelOrder', (req, res, next) => {

    const order_id = req.body._id;
    const str = 'Order Cancelled';
    const str1 = 'Order Placed'
    console.log(order_id );
    
   
  
    
    
    state.connection.query(`UPDATE orders SET _status = ? WHERE _id = ? && _status = ?`,[str, order_id, str1], function(error, items, fields) {
        
        console.log('order cancelled');
        
        res.status(200).json({
            "message": "order cancelled"
        });
        
    });
    


});



// update delivery status
router.post('/deliveryStatus', (req, res, next) => {
    const order_id = req.body._id
    const status = req.body.delivery_status
    state.connection.query("UPDATE orders SET delivery_status = ? WHERE _id = ?",[status, order_id], function(error, orders, fields) {

        console.log("Delivery status updated");
        

        res.status(201).json({
            "message": "Delivery status updated"
        });

        
        
    });
});








module.exports = router;