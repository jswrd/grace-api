const http = require('http');
const app = require('./app');

const port = process.env.PORT || 3000;  //define port for the server

const server = http.createServer(app);

server.listen(port, function(err){
    if(!err){
        
        console.log(`Server is Up now at port ${port}`);
        
    }
});